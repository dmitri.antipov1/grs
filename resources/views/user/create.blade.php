@extends('layout.admin_layout')

@section('admin_content')

    <div class="admin__container">
        @if ($errors->any())
            <div class="alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if (session()->has('success'))
            <div class="alert-success">
                {{ session('success') }}
            </div>
        @endif
        <form method="post" action="">
            @csrf
            <div class="form-group">
                <label for="name">Name</label><br>
                <input type="text" name="name" value="{{old('name')}}">
            </div>
            <div class="form-group">
                <label for="email">Email</label><br>
                <input type="email" name="email" id="email" value="{{old('email')}}">
            </div>
            <div class="form-group">
                <label for="password">Password</label><br>
                <input type="password" id="password" name="password">
            </div>
            <div class="form-group">
                <label for="password_confirmation">Confirm password</label><br>
                <input type="password" id="password_confirmation" name="password_confirmation">
            </div>
            <button type="submit">Отправить</button>
        </form>
    </div>

@endsection
