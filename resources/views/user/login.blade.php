@extends('layout.admin_layout')

@section('admin_content')

    <div class="admin__container">
        @if ($errors->any())
            <div class="alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if (session()->has('success'))
            <div class="alert-success">
                {{ session('success') }}
            </div>
        @endif
        <form method="post">
            @csrf
            <div class="form-group">
                <label for="email">Email</label><br>
                <input type="email" name="email" value="{{old('email')}}">
            </div>
            <div class="form-group">
                <label for="password">Password</label><br>
                <input type="password" name="password" value="{{old('password')}}">
            </div>
            <button type="submit">Войти</button>
        </form>
    </div>

@endsection
