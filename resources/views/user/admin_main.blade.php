@extends('layout.admin_layout')

    @section('admin_content')

            <div class="admin__header-inner">
                <div class="admin__container">
                <div class="admin__header__menu">
                    <ul class="admin__header__menu-list">
                        <li><a href="{{route('crudnews.index')}}">Новости</a></li>
                        <li><a href="{{route('crudprices.index')}}">Виды работ</a></li>
                        <li><a href="{{route('logout')}}">Выход</a></li>
                    </ul>
                    <h1 class="admin-main__login">Добро пожаловать в панель администратора. Здесь вы можете изменять некоторые блоки вашего сайта</h1>
                </div>

                </div>

            </div>


    @endsection
