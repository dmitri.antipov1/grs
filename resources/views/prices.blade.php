@extends('layout.layout')

@section('content')

    <section class="prices">
        <div class="prices__wrapper">
            <div class="prices__wrapper-title">{{ __('messages.hinnakiri') }}</div>
            <table>
                <tr>
                    <th>{{ __('messages.too') }}</th>
                    <th>{{ __('messages.hind') }}</th>
                </tr>
                @foreach($servis as $item)
                <tr>
                    @if(\Illuminate\Support\Facades\App::isLocale('en'))
                    <td>{{$item->services_ee}}</td>
                    <td>{{$item->price}}</td>
                    @elseif(\Illuminate\Support\Facades\App::isLocale('ru'))
                        <td>{{$item->services_ru}}</td>
                        <td>{{$item->price}}</td>
                    @endif
                </tr>
                @endforeach
            </table>
        </div>
    </section>
    <div class="topButton" id="topButton">
        <span class="topButton-arrow">&#171;</span>
    </div>

@endsection


