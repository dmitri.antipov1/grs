@extends('layout.layout')

@section('content')

    <section class="reservation">
        <div class="reservation__wrapper">
            @if ($errors->any())
                <div class="alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session()->has('error'))
                <div class="alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (session()->has('success'))
                <div class="alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <form action="{{ route('reserve.store') }}" method="post">
                @csrf
                <h4>Reserve time</h4>
                <div>
                    <input type="text" name="name" placeholder="Name*">
                </div>
                <div>
                    <input type="tel" name="phone" placeholder="Phone*">
                </div>
                <div>
                    <input type="email" name="email" placeholder="Email*">
                </div>
                <div>
                    <p>Choose service</p>
                    <select name="service" id="">
                        <option value="Diagnostic">Diagnostic</option>
                        <option value="Electric">Electric</option>
                        <option value="Whell changes">Whell changes</option>
                        <option value="Just talk">Just talk</option>
                    </select>
                </div>
                <div>
                    <input type="text">
                    <input name="date" placeholder="Choose date" class="textbox-n" type="text" onfocus="(this.type='date')"
                        id="date">
                </div>
                <div>
                    <p>Choose time</p>
                    <select name="time" id="">
                        <option value="9:00 - 10:00">9:00 - 10:00</option>
                        <option value="12:00 - 13:00">12:00 - 13:00</option>
                        <option value="14:00 - 15:00">14:00 - 15:00</option>
                        <option value="16:00 - 17:00">16:00 - 17:00</option>
                    </select>
                </div>
                <div>
                    <p>Get message</p>
                    <textarea name="message" id="" rows="7"></textarea>
                </div>
                <button type="submit">Send</button>
            </form>
        </div>
    </section>


@endsection
