<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin page</title>
    <link rel="icon" type="image/png" href="{{asset('images/favicon.ico')}}"/>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<section class="admin__header">
    <div class="admin__header-container">
        <h2 class="admin__header-title">Admin Panel</h2>
        <div class="admin__header__menu-logo">
            <a href="{{route('index')}}"><img  src="{{asset('images/logo.png')}}" alt=""></a>
        </div>
    </div>
    <div class="admin__header-auth">
{{--        <p><a href="{{route('register.create')}}">Регистрация</a></p>--}}
        <p><a href="{{route('login.form')}}">Войти</a></p>
    </div>
</section>
@yield('admin_content')



<footer class="footer">
    <div class="footer__title">
        &copy; <?php echo date('Y') ?> GRS Servis OÜ
    </div>
</footer>
<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
