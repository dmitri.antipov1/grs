<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="{{asset('images/favicon.ico')}}"/>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <title>GRS Servis</title>
</head>
<body>
<header class="header">
    <div class="header__wrapper">
        <div class="header__info">
            <span><img src="{{asset('images/phone.svg')}}" alt=""><a href="tel:+37256689585">+37256689585</a></span>
            <span><img src="{{asset('images/envelope.svg')}}" alt=""><a href="mailto:webmaster@example.com">info.grsservis@gmail.com</a></span>
            <span><img src="{{asset('images/clock.svg')}}" alt="">E-R 9:00 - 18:00 <br>L-P {{ __('messages.kokkuleppel') }}</span>
            <span>
                    <a href="{{ route('locale', 'en') }}"><img src="{{asset('images/estonia.svg')}}" alt=""></a>
                    <a href="{{ route('locale', 'ru') }}"><img src="{{asset('images/russia.svg')}}" alt=""></a>
                </span>
        </div>
        <div class="header__menu">
            <div class="header__menu-logo">
                <a href="{{route('index')}}"><img src="{{asset('images/logo.png')}}" alt=""></a>
            </div>

            <div class="header__menu-list animate__animated animate__backInLeft" id="header__menu-list">
                <div class="header__menu-burger" id="header__menu-burger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                @if(request()->is('/'))
                <ul>
                    <a href="#about">
                        <li class="menu-dots">{{ __('messages.meist') }}</li>
                    </a>
                    <a href="#service">
                        <li class="menu-dots">{{ __('messages.teenused') }}</li>
                    </a>
                    <a href="#news">
                        <li class="menu-dots">{{ __('messages.uudised') }}</li>
                    </a>
                    <a href="{{ route('servis') }}">
                        <li class="menu-dots">{{ __('messages.hinnakiri') }}</li>
                    </a>
                    {{-- <a href="#team">
                        <li>Team</li>
                    </a> --}}
                    <a href="#contact">
                        <li class="menu-dots">{{ __('messages.kontakt') }}</li>
                    </a>
                </ul>
                @elseif(!request()->is('/servis'))
                    <ul>
                        <a  href="{{route('index')}}"><li class="menu-dots" >{{ __('messages.back') }}</li></a>
                    </ul>
                    @endif
            </div>
        </div>

@yield('content')
        <div class="contact__info">
            <div class="contact__info-items">
                <div class="contact__info-title">
                    {{ __('messages.sotsiaalsed') }}
                </div>
                <div class="contact__info-item">
                    <img src="{{asset('images/instagram.svg')}}" alt="">
                    <div class="contact__info-text">
                        <a target="_blank" href="https://www.instagram.com/explore/locations/109923227332646/grs-servis/">
                            Instagram
                        </a>
                    </div>
                </div>
                <div class="contact__info-item">
                    <img src="{{asset('images/facebook.svg')}}" alt="">
                    <a href="https://www.facebook.com/GRSServis" target="_blank"><div class="contact__info-text">Facebook</div></a>
                </div>
            </div>
            <div class="contact__info-items">
                <div class="contact__info-title">
                    {{ __('messages.tooaeg') }}
                </div>
                <div class="contact__info-item">
                    <img src="{{asset('images/clock.svg')}}" alt="">
                    <div class="contact__info-text">E-R 9:00 - 18:00 <br> L-P {{ __('messages.kokkuleppel') }}</div>
                </div>
            </div>
            <div class="contact__info-items">
                <div class="contact__info-title">
                    {{ __('messages.kontaktid') }}
                </div>
                <div class="contact__info-item">
                    <img src="{{asset('images/location.svg')}}" alt="">
                    <div class="contact__info-text">Kantsi 4b</div>
                </div>
                <div class="contact__info-item">
                    <img src="{{asset('images/envelope.svg')}}" alt="">
                    <div class="contact__info-text"><a href="mailto:info.grsservis@gmail.com">info.grsservis@gmail.com</a></div>
                </div>
                <div class="contact__info-item">
                    <img src="{{asset('images/phone.svg')}}" alt="">
                    <div class="contact__info-text"><a href="tel:+37256689585">+37256689585</a></div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <footer class="footer">
        <div class="footer__title">
            &copy; <?php echo date('Y') ?> GRS Servis OÜ
        </div>
    </footer>

<script src="{{asset('js/app.js')}}"></script>

<script>
        let menu = document.getElementById("header__menu-list")
        let burger = document.getElementById("header__menu-burger")
        const toggleMenu = function(){
            menu.classList.toggle('active');
        }

        burger.addEventListener('click', function(e){
            e.stopPropagation();
            toggleMenu();
        });

        document.addEventListener('click', function (e){
            const target = e.target;
            const its_menu = target == menu || menu.contains(target);
            const its_burger = target == burger;
            const menu_is_active = menu.classList.contains('active');
            if (!its_menu && !its_burger && menu_is_active){
                toggleMenu();
            }
        })



    document.querySelector('#topButton').addEventListener('click', () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        })
    });

</script>
{{--    .classList.toggle('active');--}}

</body>
</html>
