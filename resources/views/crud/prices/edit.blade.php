@extends('layout.admin_layout')

@section('admin_content')
    <div class="admin__container">
        <div class="back-btn">
            <a href="{{route('crudprices.index')}}">Назад</a>
        </div>
        <form action="{{route('crudprices.update', $price->id)}}" method="post" class="form__inner">
            @if ($errors->any())
                <div class="alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session()->has('error'))
                <div class="alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (session()->has('success'))
                <div class="alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @method('PUT')
            @csrf
                <div class="form-group">
                    <label for="services_ru">Название услуги на русском</label><br>
                    <input type="text" name="services_ru">
                </div>
                <div class="form-group">
                    <label for="services_ee">Название услуги на эстонском</label><br>
                    <input type="text" name="services_ee">
                </div>
                <div class="form-group">
                    <label for="price">Цена</label><br>
                    <input type="text" name="price">
                </div>


                <button type="submit">Изменить услугу</button>
        </form>
    </div>

@endsection
