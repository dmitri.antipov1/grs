@extends('layout.admin_layout')

@section('admin_content')
    <div class="admin__container">
        <div class="back-btn">
            <a href="{{route('admin.main')}}">Назад</a>
        </div>
        <section class="admin-prices">
            <div class="admin-prices__wrapper">
                <div class="admin-prices__wrapper-title">Изменить цены и добавить услуги</div>
                <table class="admin-prices__table">
                    <tr>
                        <th>Услуги на русском языке</th>
                        <th>Услуги на эстонском языке</th>
                        <th>Цены</th>
                        <th>Исправить</th>
                        <th>Удалить</th>
                    </tr>
                    @foreach($prices as $price)
                        <tr>
                            <td>{{$price->services_ru}}</td>
                            <td>{{$price->services_ee}}</td>
                            <td>{{$price->price}}</td>
                            <td>
                                <a href="{{route('crudprices.edit', $price->id)}}">Изменить</a>
                            </td>
                            <td>
                                <form method="post" action="{{route('crudprices.destroy', $price->id)}}">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Удалить" class="form-del">
                                </form>
                            </td>
                        </tr>

                    @endforeach
                </table>
            </div>
        </section>
        @if ($errors->any())
            <div class="alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if (session()->has('success'))
            <div class="alert-success">
                {{ session('success') }}
            </div>
        @endif
        <form action="{{route('crudprices.store')}}" method="post" class="form__inner">

            @csrf

            <div class="form-group">
                <label for="services_ru">Название услуги на русском</label><br>
                <input type="text" name="services_ru">
            </div>
            <div class="form-group">
                <label for="services_ee">Название услуги на эстонском</label><br>
                <input type="text" name="services_ee">
            </div>
            <div class="form-group">
                <label for="price">Цена</label><br>
                <input type="text" name="price">
            </div>


            <button type="submit">Добавить услугу</button>
        </form>
    </div>
    @endsection
