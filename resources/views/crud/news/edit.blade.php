@extends('layout.admin_layout')

@section('admin_content')
    <div class="admin__container">
        <div class="back-btn">
            <a href="{{route('crudnews.index')}}">Назад</a>
        </div>
        <form action="{{route('crudnews.update', ['crudnews' => $news->id])}}" method="post" class="form__inner">
            @if ($errors->any())
                <div class="alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session()->has('error'))
                <div class="alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (session()->has('success'))
                <div class="alert-success">
                    {{ session('success') }}
                </div>
            @endif
                @method('PUT')
            @csrf
            <div class="form-group">
                <label for="title_ru">Заголовок на русском</label><br>
                <input type="text" name="title_ru" value="">
            </div>
            <div class="form-group">
                <label for="text_ru">Новость на русском</label><br>
                <textarea rows="5" name="text_ru" id="text_ru"></textarea>
            </div>
            <div class="form-group">
                <label for="title_ee">Заголовок на эстонском</label><br>
                <input type="text" name="title_ee" >
            </div>
            <div class="form-group">
                <label for="text_ee">Новость на эстонском</label><br>
                <textarea rows="5" name="text_ee" id="text_ee"></textarea>
            </div>

            <button type="submit">Изменить</button>
        </form>
    </div>

@endsection
