@extends('layout.admin_layout')

@section('admin_content')
    <div class="admin__container">
        <div class="back-btn">
            <a href="{{route('admin.main')}}">Назад</a>
        </div>
        <div class="news__table-inner">
            <table>
                <h1>Список новостей</h1>
                @if ($errors->any())
                    <div class="alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session()->has('error'))
                    <div class="alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session()->has('success'))
                    <div class="alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <tr>
                    <th>Заголовок на русском языке</th>
                    <th>Новость на русском языке</th>
                    <th>Заголовок на эстонском языке</th>
                    <th>Новость на эстонском языке</th>
                    <th>Дата публикации</th>
                    <th>Изменить</th>
                    <th>Удалить</th>
                </tr>
                @foreach($crud_news as $item)
                    <tr>
                        <td>{{$item->title_ru}}</td>
                        <td>{{$item->text_ru}}</td>
                        <td>{{$item->title_ee}}</td>
                        <td>{{$item->text_ee}}</td>
                        <td>{{$item->created_at}}</td>
                        <td>
                            <a href="{{route('crudnews.edit', ['crudnews' => $item->id])}}">Изменить</a>
                        </td>
                        <td>
                            <form method="post" action="{{route('crudnews.destroy', ['crudnews' => $item->id])}}">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Удалить" class="form-del">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <form action="{{route('crudnews.store')}}" method="post" class="form__inner">

            @csrf
            <div class="form-group">
                <label for="title_ru">Заголовок на русском</label><br>
                <input type="text" name="title_ru" value="{{old('title_ru')}}">
            </div>
            <div class="form-group">
                <label for="text_ru">Новость на русском</label><br>
                <textarea rows="5" name="text_ru" id="text_ru"></textarea>
            </div>
            <div class="form-group">
                <label for="title_ee">Заголовок на эстонском</label><br>
                <input type="text" name="title_ee" value="{{old('title_ee')}}">
            </div>
            <div class="form-group">
                <label for="text_ee">Новость на эстонском</label><br>
                <textarea rows="5" name="text_ee" id="text_ee"></textarea>
            </div>

            <button type="submit">Добавить новость</button>
        </form>

    </div>

@endsection
