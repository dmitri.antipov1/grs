@extends('layout.layout')

@section('content')


{{--    <div class="topButton" id="topButton">--}}
{{--        <span class="animate__animated animate__slower	3s animate__fadeInUp">&#8657;</span>--}}
{{--    </div>--}}
<div class="topButton animate__animated animate__slower	3s animate__fadeInUp" id="topButton">
    <span class="topButton-arrow ">&#171;</span>
</div>
    <div class="header__bg-image">
        <img src="{{ asset('images/background3.jpg') }}" alt="">
        <div class="header__title">{{ __('messages.prof_tood') }}</div>
    </div>
    </div>
    </header>

    <section class="slider_section" id="news">
        <div class="slider__title">
            {{ __('messages.uudised') }}
        </div>
        <div class="slider_wrapper">
            <div class="slider">
                @foreach($news as $item)
                <div class="slider__item">
                    @if(\Illuminate\Support\Facades\App::isLocale('en'))
                    <div class="slider__item-title">{{$item->title_ee}}</div>
                    <div class="slider__item-text">{{$item->text_ee}}</div>
                    @elseif(\Illuminate\Support\Facades\App::isLocale('ru'))
                        <div class="slider__item-title">{{$item->title_ru}}</div>
                        <div class="slider__item-text">{{$item->text_ru}}</div>
                        @endif
                </div>
                @endforeach
            </div>
        </div>

    </section>

    <section class="about" id="about">

        <div class="about__title">
            {{ __('messages.meist') }}
        </div>
        <div class="container">
            <div class="about__wrapper">
                <div class="about__box wow animate__animated animate__bounceInLeft" data-wow-offset="300">
                    <div class="about__box-img">
                        <img src="{{ asset('images/wrench.svg') }}" alt="">
                    </div>
                    <div class="about__box-description">
                        <div class="about__box-title">{{ __('messages.kogemus') }}</div>
                        <div class="about__box-text">{{ __('messages.descr1') }}</div>
                    </div>
                </div>
                <div class="about__box about__box-rev wow animate__animated animate__bounceInLeft" data-wow-offset="300">
                    <div class="about__box-img">
                        <img src="{{ asset('images/euro.svg') }}" alt="">
                    </div>
                    <div class="about__box-description">
                        <div class="about__box-title">{{ __('messages.hinnad') }}</div>
                        <div class="about__box-text">{{ __('messages.descr2') }}</div>
                    </div>
                </div>
                <div class="about__box wow animate__animated animate__bounceInLeft" data-wow-offset="300">
                    <div class="about__box-img">
                        <img src="{{ asset('images/car-service.svg') }}" alt="">
                    </div>
                    <div class="about__box-description">
                        <div class="about__box-title">{{ __('messages.garantii') }}</div>
                        <div class="about__box-text">{{ __('messages.descr3') }}</div>
                    </div>
                </div>

            </div>

        </div>
    </section>



    <section class="service" id="service">
        <div class="container">
            <div class="service__title">{{ __('messages.teenused') }}</div>
            <div class="service__items-wrapper">
                <div class="service__item">
                    <div class="service__item-img">
                        <img src="{{ asset('images/service-1.jpg') }}" alt="">
                    </div>
                    <div class="service__item-title wow animate__animated animate__bounceInRight">{{ __('messages.diagnostika') }}</div>
                </div>
                <div class="service__item">
                    <div class="service__item-img">
                        <img src="{{ asset('images/service-2.jpg') }}" alt="">
                    </div>
                    <div class="service__item-title wow animate__animated animate__bounceInRight">{{ __('messages.olehavetus') }}</div>
                </div>
                <div class="service__item">
                    <div class="service__item-img">
                        <img src="{{ asset('images/service-3.jpg') }}" alt="">
                    </div>
                    <div class="service__item-title wow animate__animated animate__bounceInRight">{{ __('messages.motoor_remont') }}</div>
                </div>
                <div class="service__item">
                    <div class="service__item-img">
                        <img src="{{ asset('images/service-4.jpg') }}" alt="">
                    </div>
                    <div class="service__item-title wow animate__animated animate__bounceInRight">{{ __('messages.tulede_pol') }}</div>
                </div>
                <div class="service__item">
                    <div class="service__item-img">
                        <img src="{{ asset('images/service-5.jpg') }}" alt="">
                    </div>
                    <div class="service__item-title wow animate__animated animate__bounceInRight">{{ __('messages.kere_tood') }}</div>
                </div>
                <div class="service__item">
                    <div class="service__item-img">
                        <img src="{{ asset('images/service-6.jpg') }}" alt="">
                    </div>
                    <div class="service__item-title wow animate__animated animate__bounceInRight">{{ __('messages.veermiku_rem') }}</div>
                </div>
            </div>
            <div class="about__button">
                <a href="{{ route('servis') }}">{{ __('messages.hinnakiri') }}</a>
            </div>
        </div>
    </section>

    {{-- <section class="reserve" id="reserve">
        <div class="container">
            <div class="reserve__title">Reserve</div>
            <div class="reserve__btn">
                <a href="{{ route('reserve.index') }}">Book now!</a>
            </div>
        </div>
    </section> --}}

    {{-- <section class="team" id="team">
        <div class="container">
            <div class="team__title">
                Team
            </div>
            <div class="team__wrapper">
                <div class="team__item wow animate__animated animate__bounceInLeft">
                    <div class="team__item-name">
                        Julian
                    </div>
                    <div class="team__item-foto">
                        <img src="{{ asset('images/worker.png') }}" alt="">
                    </div>
                    <div class="team__item-social">
                        <a href="/"><img src="{{ asset('images/instagram.svg') }}" alt=""></a>
                        <a href="/"><img src="{{ asset('images/instagram.svg') }}" alt=""></a>
                        <a href="/"><img src="{{ asset('images/facebook.svg') }}" alt=""></a>
                    </div>
                </div>
                <div class="team__item wow animate__animated animate__bounceInLeft">
                    <div class="team__item-name">
                        Julian
                    </div>
                    <div class="team__item-foto">
                        <img src="{{ asset('images/worker.png') }}" alt="">
                    </div>
                    <div class="team__item-social">
                        <a href="/"><img src="{{ asset('images/instagram.svg') }}" alt=""></a>
                        <a href="/"><img src="{{ asset('images/instagram.svg') }}" alt=""></a>
                        <a href="/"><img src="{{ asset('images/facebook.svg') }}" alt=""></a>
                    </div>
                </div>
                <div class="team__item wow animate__animated animate__bounceInLeft ">
                    <div class="team__item-name">
                        Julian
                    </div>
                    <div class="team__item-foto">
                        <img src="{{ asset('images/worker.png') }}" alt="">
                    </div>
                    <div class="team__item-social">
                        <a href="/"><img src="{{ asset('images/instagram.svg') }}" alt=""></a>
                        <a href="/"><img src="{{ asset('images/instagram.svg') }}" alt=""></a>
                        <a href="/"><img src="{{ asset('images/facebook.svg') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}

    <section class="contact" id="contact">
        <div class="container">
            <div class="contact__title ">
                {{ __('messages.kontakt') }}
            </div>
            <div class="contact__wrapper">
                <form action="{{ route('form') }}" method="post">
                    @if ($errors->any())
                        <div class="alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('error'))
                        <div class="alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session()->has('success'))
                        <div class="alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @csrf
                    <input name="name" type="text" placeholder="{{ __('messages.nimi') }}*" required>
                    <input name="phone" type="telephone" placeholder="{{ __('messages.telefon') }}*" required>
                    <input name="email" type="email" placeholder="{{ __('messages.email') }}*" required>
                    <textarea name="message" rows="5" placeholder="{{ __('messages.kirjeldus') }}*" required></textarea>
                    <input type="submit" value="{{ __('messages.saada') }}"
                        class="button"
                        onclick="return confirm('Send message?')">
                </form>
            </div>
            <div class="contact__map">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2870.0848714124304!2d24.8194478202296!3d59.42546108222956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4692eca6499051e5%3A0xccbd52f2956952b4!2sKantsi%204b%2C%2011415%20Tallinn!5e0!3m2!1sru!2see!4v1601496866846!5m2!1sru!2see"></iframe>
            </div>

        @endsection
