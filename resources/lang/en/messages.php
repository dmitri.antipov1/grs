<?php

return [
    'kokkuleppel' => 'Kokkuleppel',
    'meist' => 'Meist',
    'teenused' => 'Teenused',
    'uudised' => 'Uudised',
    'hinnakiri' => 'Hinnakiri',
    'kontakt' => 'Kontakt',
    'sotsiaalsed' => 'Sotsiaalsed',
    'tooaeg' => 'Tööaeg',
    'kontaktid' => 'Kontaktid',
    'prof_tood' => 'professionaalsed teenused teie autole',
    'kogemus' => 'Meistrite kogemus',
    'descr1' => 'Meie spetsialistide kogemus ja professionaalsus võimaldavad meil võimalikult keerulise tehnilise probleemi kvalitatiivselt lahendada võimalikult lühikese aja jooksul.',
    'hinnad' => 'Parimad hinnad',
    'descr2' => 'Meie auto remonti kogemus ei tähenda kalleid hindu. Alati soodsad teenindus-, diagnostika- ja remondihinnad.',
    'garantii' => 'Garantii',
    'descr3' => 'Meie autoteenindus annab garantii remonditöödele, kasutades originaalvaruosi.',
    'diagnostika' => 'Diagnostika',
    'olehavetus' => 'Õlehavetus',
    'motoor_remont' => 'Mootori remont',
    'tulede_pol' => 'Rehvitööd',
    'kere_tood' => 'Kere tööd',
    'veermiku_rem' => 'Veermiku remont',
    'nimi' => 'Nimi',
    'telefon' => 'Telefon',
    'email' => 'Email',
    'kirjeldus' => 'Kirjeldus',
    'saada' => 'Saada',
    'too' => 'Töö',
    'hind' => 'Hind',
    'back' => 'Tagasi'

];

?>
