<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/locale/{locale}', 'IndexController@changeLocale')->name('locale');

Route::group(['middleware' => ['set_locale']], function () {
    Route::get('/', 'IndexController@showIndex')->name('index');
    Route::post( '/', 'IndexController@indexForm')->name('form');
    Route::get('/servis', 'ServisController@show')->name('servis');
});

Route::get('/register', 'UserController@create')->name('register.create');
Route::post('/register', 'UserController@store')->name('register.store');

Route::get('/login', 'UserController@loginForm')->name('login.form');
Route::post('/login', 'UserController@login')->name('login');
Route::get('/logout', 'UserController@logout')->name('logout');




Route::group(['middleware' => 'auth'], function (){
    Route::get('/main', function(){return view('user.admin_main');})->name('admin.main');
    Route::resource('crudnews', 'NewsController');
    Route::resource('crudprices', 'PriceController');
});



