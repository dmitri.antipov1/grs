<?php

namespace App\Http\Controllers;

use App\Models\Prices;
use Illuminate\Http\Request;

class ServisController extends Controller
{
   public function show(){

       $servis = Prices::all();

       return view('prices', compact('servis'));
   }
}
