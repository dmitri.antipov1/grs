<?php

namespace App\Http\Controllers;

use App\Mail\ConnectMail;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller
{
    public function showIndex()
    {
        $news = News::query()->latest('created_at')->limit(3)->get();
//            DB::table('news')->orderBy('text_ee', 'desc')->limit(2)->first();

        return view('index', compact('news'));
    }

    public function indexForm(Request $request)
    {
        if ($request->method() == 'POST'){

            $request->validate([
                'name' => 'required',
                'phone' => 'required|numeric',
                'email' => 'required|email',
                'message' => 'required',
            ]);

            $body = "<p><b>Имя:</b></p> {$request->input('name')}";
            $body .= "<p><b>Телефон:</b></p> {$request->input('phone')}";
            $body .= "<p><b>Email:</b></p> {$request->input('email')}";
            $body .= "<p><b>Сообщение:</b><br>" . nl2br($request->input('message')) ."</p>";
            Mail::to('info.grsservis@gmail.com')->send(new ConnectMail($body)); //here add grs info mail
            $request->session()->flash('success', 'Message has send');
            return redirect('/#contact');
        }
        return redirect('/#contact');
    }

        public function changeLocale($locale){

            session(['locale' => $locale]);
            App::setLocale($locale);
           return redirect()->back();
        }



}
