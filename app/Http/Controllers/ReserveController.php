<?php

namespace App\Http\Controllers;

use App\Mail\ConnectMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ReserveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('reserve');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       if ($request->method() == 'POST'){

            $request->validate([
                'name' => 'required',
                'phone' => 'required|numeric',
                'email' => 'required|email',
                'service' => 'required',
                'date' => 'required',
                'time' => 'required',
                'message' => 'required',
            ]);

            $data = "<p><b>Name</b></p> {$request->input('name')}";
            $data .= "<p><b>Phone</b></p> {$request->input('phone')}";
            $data .= "<p><b>Email</b></p> {$request->input('email')}";
            $data .= "<p><b>Service</b></p> {$request->input('service')}";
            $data .= "<p><b>Date</b></p> {$request->input('data')}";
            $data .= "<p><b>Time</b></p> {$request->input('time')}";
            $data .= "<p><b>Message:</b><br>" . nl2br($request->input('message')) ."</p>";
            Mail::to('dmitri.antipov1@gmail.com')->send(new ConnectMail($data)); //here add grs info mail
            $request->session()->flash('success', 'Message has send');
            return redirect('/reserve');
        }
        return redirect('/reserve');
    }
}
