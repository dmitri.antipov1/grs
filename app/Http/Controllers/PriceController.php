<?php

namespace App\Http\Controllers;

use App\Models\Prices;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prices = Prices::all();
        return view('crud.prices.index', compact('prices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'services_ru' => 'required',
            'services_ee' => 'required',
            'price' => 'required',
        ]);

        Prices::create($request->all());

        return redirect()->route('crudprices.index')->with('success', 'Добавлено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $price = Prices::find($id);
        return view('crud.prices.edit', [
            'price' => $price,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'services_ru' => 'required',
            'services_ee' => 'required',
            'price' => 'required',
        ]);

        $price = Prices::find($id);
        $price->update($request->all());
        return redirect()->route('crudprices.index')->with('success', 'Изменено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Prices::destroy($id);
        return redirect()->route('crudprices.index')->with('success', 'Удалено!');
    }
}
