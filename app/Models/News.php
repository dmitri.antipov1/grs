<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @package App\Models
 * @mixin Builder
 */
class News extends Model
{
    use HasFactory;

    protected $table = 'news';
    protected $fillable = ['title_ee', 'title_ru', 'text_ee', 'text_ru'];
}
